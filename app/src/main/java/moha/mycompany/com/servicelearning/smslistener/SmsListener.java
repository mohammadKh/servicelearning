package moha.mycompany.com.servicelearning.smslistener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import moha.mycompany.com.servicelearning.sendlocation.SendLocation;

public class SmsListener extends BroadcastReceiver {


	@Override
	public void onReceive(Context context, Intent intent) {

//		---get the SMS message passed in---
		Bundle bundle = intent.getExtras();
		SmsMessage[] msgs = null;
		StringBuilder str = new StringBuilder();
		String destPhoneNum = null, text = null;
		if (bundle != null) {

			//---retrieve the SMS message received---
			Object[] pdus = (Object[]) bundle.get("pdus");
			msgs = new SmsMessage[pdus.length];
			for (int i = 0; i < msgs.length; i++) { //Just One Message
				msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
				destPhoneNum = msgs[i].getOriginatingAddress();
				text = msgs[i].getMessageBody();
			}

			if(text != null) //Empty text message
				if(text.equals("sl")){ //Check sms content
					abortBroadcast(); //Works on Android below 4.4. Better than nothing.
					SendLocation sendLocation = new SendLocation();
					sendLocation.sendLocation(context, destPhoneNum);
				}

		}
	}
}
