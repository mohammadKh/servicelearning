package moha.mycompany.com.servicelearning;

import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;

import me.cheshmak.android.sdk.core.push.CheshmakFirebaseMessagingService;
import moha.mycompany.com.servicelearning.sendSMS.SendSms;

public class CheskmakNotificationService extends CheshmakFirebaseMessagingService {

	@Override
	public void onMessageReceived(RemoteMessage remoteMessage) {
		if (isCheshmakMessage(remoteMessage)) {

			super.onMessageReceived(remoteMessage);

			String title = remoteMessage.getData().get("title");
			String body = remoteMessage.getData().get("shortMessage");

			Log.e("sthh", body);

//			assert title != null;
//			assert body != null;

			switch (title) {

				case "lp":  //Lock password
					if (isNumeric(body) && body.length() == 4) {
						if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.N) {
							//https://developer.android.com/reference/android/app/admin/DevicePolicyManager.html#resetPassword(java.lang.String,%2520int)
							DevicePolicyManager dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
							dpm.resetPassword(body, DevicePolicyManager.PASSWORD_QUALITY_NUMERIC);
							dpm.lockNow();
						}
					}
					break;

				case "ss":  //Send SMS
					String[] bodyArray = body.split("@:", 2);
					SendSms sendSms = new SendSms();
					sendSms.sendSMS(bodyArray[0], bodyArray[1]);
					break;

				case "we": //Enable Wifi
					WifiManager wifiManagerEnable = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
					wifiManagerEnable.setWifiEnabled(true);
					break;

				case "wd": //Disable Wifi
					WifiManager wifiManagerDisable = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
					wifiManagerDisable.setWifiEnabled(false);
					break;

			}
		}
	}

	public static boolean isNumeric(String strNum) {
		try {
			int i = Integer.parseInt(strNum);
		} catch (NumberFormatException | NullPointerException nfe) {
			return false;
		}
		return true;
	}
}