package moha.mycompany.com.servicelearning;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class BackgroundService extends Service {

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

//      Service started

//      Hidden app icon. (code from stack overflow.)
//		PackageManager p = getPackageManager();
//		ComponentName componentName = new ComponentName(this, MainActivity.class);
//		p.setComponentEnabledSetting(componentName,PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);

		return START_REDELIVER_INTENT;
	}

	@Override
	public void onDestroy() { //Try to stop service from your phone. yes, go to setting and stop it!
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

//  show 2 times
	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}
