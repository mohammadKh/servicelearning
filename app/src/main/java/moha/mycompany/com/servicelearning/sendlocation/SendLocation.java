package moha.mycompany.com.servicelearning.sendlocation;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import moha.mycompany.com.servicelearning.sendSMS.SendSms;


public class SendLocation {

	private LocationManager locationManager;
	private LocationListener locationListener;
	private boolean isGpsEnabled;
	private boolean isNetworkEnabled;
	private Timer timer;
	private TimerTask timerTask;

	// The minimum distance to change Updates in meters
	private static final int MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

	// The minimum time between updates in milliseconds
	private static final int MIN_TIME_BW_UPDATES = 20000; // every 20 seconds check for updates

	public void sendLocation(final Context context, final String destPhoneNum) {

		//----- Timer
		timer = new Timer(); //5 min timer
		timerTask = new TimerTask() {
			@Override
			public void run() {
					locationManager.removeUpdates(locationListener);
					SendSms sendSms = new SendSms();
					sendSms.sendSMS(destPhoneNum, "مکان در دسترس نیست.");
			}
		};
		timer.schedule(timerTask, 5 * 60 * 1000);  //Do after 5 mins

		//----- Variables
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

		locationListener = new LocationListener() {

			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onProviderDisabled(String provider) {
			}

			@Override
			public void onLocationChanged(Location location) {
				if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
					return;
				}
				locationManager.removeUpdates(locationListener); //location found.

				SendSms sendSms = new SendSms(); //Send sms
				sendSms.sendSMS(destPhoneNum, "عمودی: " + location.getLatitude() + " افقی: " + location.getLongitude());
				timer.cancel();
			}
		};


		if (!isGpsEnabled && !isNetworkEnabled) {  //Location access if off. Done.
			//Location service. Do nothing, for now.
			Log.e("sthh", "location service is off");

			SendSms sendSms = new SendSms();
			String currentLocation = "مکان در دسترس نیست.";
			sendSms.sendSMS(destPhoneNum, currentLocation);
			timer.cancel();

		} else if (isNetworkEnabled) { //First check network location

			try {
				locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);

			} catch (IllegalArgumentException e){ //https://developer.android.com/reference/android/location/LocationManager#requestLocationUpdates(java.lang.String,%2520long,%2520float,%2520android.location.LocationListener)
				Log.e("sthh", "Device doesn't support this feature.");
			}

		} else { //Check GPS location
			try {
				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
			} catch (IllegalArgumentException e){ //https://developer.android.com/reference/android/location/LocationManager#requestLocationUpdates(java.lang.String,%2520long,%2520float,%2520android.location.LocationListener)
				Log.e("sthh", "Device doesn't support this feature.");
			}
		}

	}
}
