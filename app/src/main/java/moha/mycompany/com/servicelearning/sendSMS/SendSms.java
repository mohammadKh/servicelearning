package moha.mycompany.com.servicelearning.sendSMS;

import android.telephony.SmsManager;


public class SendSms {


	public void sendSMS(String phoneNumber, String message) {
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(phoneNumber, null, message, null, null);
	}

}
